import json
import sys

with open(sys.argv[1], 'r') as f:
    resource_json = json.load(f)
location = (resource_json["testbed"][0])["testbed"]["location"]
print(location["continent"]+"."+location["country"])
