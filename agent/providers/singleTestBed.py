{
   "testbed": [
      {
         "testbed": {
            "description": "",
            "id": "wilab2",
            "location": {"country":"USA", "continent":"AMERICA"},
            "cost_per_cluster": [
                {"cluster": "ZOTAC", "price": 0.010},
                {"cluster": "APU 1d4", "price": 0.015},
                {"cluster": "DSS/MOBILE", "price": 0.017},
                {"cluster": "SERVER1P", "price": 0.12},
                {"cluster": "SERVER5P", "price": 0.21},
                {"cluster": "SERVER1G2X", "price": 0.11}
             ],
            "dc-slice-controller" :
                 {"dc-slice-provider": "wilab2",
                  "ip": "165.227.5.18",
                  "port": 1090
                  },
             "dc-slice-point-of-presence" : {
                  "pop-name": "Edge Router 1",
                  "ip": "165.227.5.12",
                  "router-type": "Wired Router-cisco",
                  "reservation-protocol": "BGP",
                  "requirements":
                     {"bandwidth_gb": 1}
                },
            "nodes": [
               {
                  "node_cluster": {
                     "available_nodes": 31,
                     "cpu_cores": 2,
                     "cpu_ghz": 1.8,
                     "cpu_model": "",
                     "cpu_number": 2,
                     "cpu_vendor": "Intel",
                     "memory_mb": 4096,
                     "min_storage_gb": 160,
                     "name": "ZOTAC",
                     "nics_bw": 1,
                     "nics_description": "",
                     "rspec_hwtype": " ['ZOTAC', 'pc', 'ZOTAC-vm', 'pcvm', 'lan']",
                     "storage_description": "160",
                     "total_cpu_cores": 2,
                     "total_nodes": 45
                  }
               },
               {
                  "node_cluster": {
                     "available_nodes": 34,
                     "cpu_cores": 2,
                     "cpu_ghz": 1.0,
                     "cpu_model": "",
                     "cpu_number": 2,
                     "cpu_vendor": "AMD",
                     "memory_mb": 4096,
                     "min_storage_gb": 32,
                     "name": "APU 1d4",
                     "nics_bw": 2,
                     "nics_description": "",
                     "rspec_hwtype": " ['APU', 'pc', 'APU-vm', 'pcvm', 'lan']",
                     "storage_description": "32",
                     "total_cpu_cores": 4,
                     "total_nodes": 43
                  }
               },
               {
                  "node_cluster": {
                     "available_nodes": 10,
                     "cpu_cores": 0,
                     "cpu_ghz": 0.0,
                     "cpu_model": "",
                     "cpu_number": 0,
                     "cpu_vendor": "Intel ",
                     "memory_mb": 4096,
                     "min_storage_gb": 60,
                     "name": "DSS/MOBILE",
                     "nics_bw": 0,
                     "nics_description": "",
                     "rspec_hwtype": " ['DSS', 'pc', 'DSS-vm', 'pcvm', 'lan']",
                     "storage_description": "60",
                     "total_cpu_cores": 0,
                     "total_nodes": 10
                  }
               },
               {
                  "node_cluster": {
                     "available_nodes": 2,
                     "cpu_cores": 2,
                     "cpu_ghz": 2.8,
                     "cpu_model": "",
                     "cpu_number": 8,
                     "cpu_vendor": "Intel ",
                     "memory_mb": 12288,
                     "min_storage_gb": 160,
                     "name": "SERVER1P",
                     "nics_bw": 0,
                     "nics_description": "",
                     "rspec_hwtype": " ['SERVER1P', 'pc', 'SERVER1P-vm', 'pcvm', 'lan']",
                     "storage_description": "160",
                     "total_cpu_cores": 16,
                     "total_nodes": 2
                  }
               },
               {
                  "node_cluster": {
                     "available_nodes": 4,
                     "cpu_cores": 2,
                     "cpu_ghz": 2.8,
                     "cpu_model": "",
                     "cpu_number": 8,
                     "cpu_vendor": "",
                     "memory_mb": 12288,
                     "min_storage_gb": 160,
                     "name": "SERVER5P",
                     "nics_bw": 2,
                     "nics_description": "",
                     "rspec_hwtype": " ['SERVER5P', 'pc', 'SERVER5P-vm', 'pcvm', 'lan']",
                     "storage_description": "160",
                     "total_cpu_cores": 16,
                     "total_nodes": 5
                  }
               },
               {
                  "node_cluster": {
                     "available_nodes": 6,
                     "cpu_cores": 8,
                     "cpu_ghz": 2.1,
                     "cpu_model": "",
                     "cpu_number": 1,
                     "cpu_vendor": "Intel",
                     "memory_mb": 16384,
                     "min_storage_gb": 500,
                     "name": "SERVER1G2X",
                     "nics_bw": 0,
                     "nics_description": "",
                     "rspec_hwtype": " ['SERVER1G2X', 'pc', 'SERVER1G2X-vm', 'pcvm', 'lan']",
                     "storage_description": "500",
                     "total_cpu_cores": 8,
                     "total_nodes": 7
                  }
               }
            ]
         }
      }
   ]
}
