# Python - Prolog Connection


from pyswip import *


##############################################################################################
#### This is the Prolog Connection part.
#### Prolog Utilities
def agentAnswer(mess):
    answerAgent = Functor("answerAgent",2)
    X = Variable()
    qr = Query(answerAgent(mess,X))
    if qr.nextSolution():
        #True and False present a problem, this is why they are defined.
        ans = eval(str(X.value),{},{"true":'true',"false":'false', "null":'null'})
    else:
        print('Prolog Query Failed.')
        ans = 'None'
    qr.closeQuery()
    return ans

# Loading testbed information.

def load_testbed_info(FileName):
    load_agentdb_file = Functor("load_agentdb_file",3)
    X = Variable()
    Status = Variable()
    qr = Query(load_agentdb_file(FileName,X,Status))
    if qr.nextSolution():
        #True and False present a problem, this is why they are defined.
        print('Loading database: '+str(Status.value))
    else:
        print('Problem loading the resource database in Agent.')
    qr.closeQuery()

# Starts Prolog and consults the FileName (Prolog Code)
def start_prolog(FileName):
    pr = Prolog()
    pr.consult(FileName)
    return pr
