#!/bin/bash

FILES=`ls ./f4f_testbeds/*resources.json`
echo $FILES
for TEST in $FILES; do
  Name=$(python3 terminalName.py $TEST)
  #echo $TEST
  #echo $Name
  xterm -T $Name +ls -xrm 'XTerm*selectToClipboard: true' -hold -e python3 -B agent.py $TEST &
done
#xterm -hold
