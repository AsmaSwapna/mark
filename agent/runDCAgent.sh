#!/bin/bash

case $1 in
     "cloudlabUtah"|"cloudlabWisconsin"|"grid5000"|"vwall1"|"vwall2"|"wilab2")
        cd testbedExtractor
        python dump-all-rspecsDeamon-parser.py --Trigger --testbedName $1
        cd ..
        python3 -B agent.py $1
          ;;
      dcProv*.json)
        python3 -B agent.py "./providers/${1}"
          ;;
     *)
       #echo $1
       echo "Error: ${1} is NOT a valid testbed"
          ;;
esac


#xterm -hold
