#!/usr/bin/env python
# Marketplace Broker Main File
# Developed under the Necos Project (http://www.h2020-necos.eu/)
# Authors: Sarantis Kalafatidis, Polyxronis Valsamas, Ilias Sakellariou, Lefteris Mamatas
# University of Macedonia, Thessaloniki Greece.


from threading import Thread
from multiprocessing.pool import ThreadPool
#from functools import reduce
from broker_slicing import *
from agentCon import *
#import unicodedata, ast, json
import ast
import json
import sys, uuid, copy, pprint, yml2json, time
import pika
from marketplace_utilities import load_rabbit_config

##################################################
# signal section
import signal
def terminateAgent(f1,f2):
    sys.exit(1)

signal.signal(signal.SIGHUP, terminateAgent)
signal.signal(signal.SIGTERM, terminateAgent)
signal.signal(signal.SIGINT, terminateAgent)
###################################################

RMQ_Conf = load_rabbit_config()
hostIP = RMQ_Conf['rabbitmq_ip']
creds = pika.PlainCredentials(RMQ_Conf['rabbitmq_user'], RMQ_Conf['rabbitmq_pass'])

def main():
    '''Start a thread to connect with builder and waiting for requests'''
    t1 = Thread(name="thread1", target=connectWbuilder)
    t1.start()


def connectWbuilder():
    '''waiting messages from the builder.When you recieve a message
     create a thread and send to agents and waiting for responces '''
    #----Create and declare a unic queue for the broker---
    channel_builder.queue_declare(queue='rpc_queue')
    channel_builder.basic_consume(queue='rpc_queue', on_message_callback=respontTbuilder)
    print("***Starting connection with builder***")
    channel_builder.start_consuming()

def respontTbuilder(ch, method, props, body):
    '''Take the message from agent (message_to_builder) and send to builder'''
    print("***Received Request from builder***")
    message_to_builder = answer_builder_request(body)
    #print("***Querying Agents.***")
# change here
#def reply_to_builder(props,message_to_builder):
    channel_builder.basic_publish(exchange='',
                                  routing_key=props.reply_to,
                                  body=str(message_to_builder))
    channel_builder.basic_ack(delivery_tag=method.delivery_tag)


### Change Communication with agents is done in a function.
def answer_builder_request(body):
    d = json.loads(body)
    #print(d)
    slice_des = d["slices"]["sliced"]
    messages = [populate(spm, slice_des) for spm in d["slices"]["sliced"]["slice"] if "dc-slice-part" in spm]
    message_to_builder = []
    # Loop for the DC Messages
    print("The number of dc-slice parts in the request is: ", len(messages))
    for i in messages:
    #----Create routing key----
        key = fetch(i,["dc-slice-part", "slice-contraints", "geographic", "continent"])
        key +="."
        country = fetch(i,["dc-slice-part", "slice-contraints","geographic","country"])
        #if fetch(i,["dc-slice-part", "slice-contraints","geographic","country"]) is not None:
        if country is not None and country != "any":
            key ="."
            key += country
            #key += fetch(i,["dc-slice-part","slice-contraints","geographic","country"])
        routing_key = key # unicodedata.normalize('NFKD', key).encode('ascii','ignore')
    #----Message sending to agents----
        message = json.dumps(i)
        print ("Routing key is (geographic Constraint): ",routing_key)
        #print ("the message is: ",message)
        print('Sending message to DC agents.')
        message_received = (call(routing_key,message))
        message_to_builder.extend(message_received)
        #time.sleep(3)
        #print(message_to_builder)

    # Getting Info from answers
    dc_parts_info = [extract_info_agent_message(eval(reply,{},{"true":'true',"false":'false', "null":'null'})) \
                     for reply in message_to_builder]
    for dans in dc_parts_info:
        print(dans['dc-part-id'], ':',dans['provider'])
    # constructing the Messages
    net_messages = create_messages_to_net_agents(slice_des,dc_parts_info)
    print("Sending Message to WAN Agents.")
    # Net Message Loop Starts Here
    for net_m in net_messages:
        print(json.dumps(net_m["net-slice-part"]["link-ends"]))
        net_message_received = (call("wan",json.dumps(net_m)))
        for arepl in net_message_received:
            r = json.loads(arepl)
            print(json.dumps(r["net-slice-part"]["wan-slice-controller"]),json.dumps(r["net-slice-part"]["cost"]))
            #print(json.dumps(r["net-slice-part"]["cost"]))
        message_to_builder.extend(net_message_received)

    print("*********---DONE---***********")
    return message_to_builder




def sendToAgents(routing_key,message_from_builder):
    #this function is a thread generator for broker-agents connection
    pool = ThreadPool(processes=1)
    async_result = pool.apply_async(call,(routing_key,message_from_builder))
    print("message_to_builder",async_result)
    return async_result

def call(routing_key,message):
    start_agent_consuming = Start_agent_consuming(hostIP,creds)
    response = start_agent_consuming.call(routing_key,message)
    #IS: no needed time.sleep(2)
    return response

if __name__ == "__main__":
    #---------------- Connection ----------------------
    #----connecting with RabbitMQ server----
    connection_builder = pika.BlockingConnection(
         pika.ConnectionParameters(host=hostIP, credentials = creds, heartbeat=0))
    channel_builder = connection_builder.channel()

    main()
