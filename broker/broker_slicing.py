# Slice Request Decomposition
# Developed under the Necos Project (http://www.h2020-necos.eu/)
# Authors: Ilias Sakellariou, Sarantis Kalafatidis, Polyxronis Valsamas, Lefteris Mamatas
# University of Macedonia, Thessaloniki Greece.

import copy
from functools import reduce

def populate(slice_part_message,slice_des):
    outMessage = copy.deepcopy(slice_part_message)
    print ("")
    dc_part_message = outMessage["dc-slice-part"]
    if fetch(dc_part_message,["slice-contraints","geographic"]) == None:
        slice_geo_country  = copy.deepcopy(fetch(slice_des,["slice-constraints","geographic"]))#,"country"]))
        dc_part_message.update({"slice-contraints":{"geographic":slice_geo_country}}) #{"country":slice_geo_country}}})

    dc_part_message["slice-lifecycle"] = copy.deepcopy(fetch(slice_des,["slice-lifecycle"]))
    dc_part_message["cost"] = {}
    dc_part_message["cost"]["dc-model"] = copy.deepcopy(fetch(slice_des,["cost","dc-model"]))
    # New Code for message to be send to dc slice agents.
    dc_part_message["dc-slice-point-of-presence"] = "undefined"
    part_vdus = fetch(dc_part_message,["vdus"])
    for vdu in part_vdus:
        vdu["dc-vdu"]["epa-attributes"] = copy.deepcopy(fetch_epa(fetch(vdu,["dc-vdu","id"]),slice_des))

    return outMessage


def fetch_epa(id,slice_des):
    listofservices = fetch(slice_des,["service"])
    for i in [ x for x in listofservices if  "service-function" in x] :#fetch(x,["service-function","service-element-type"]) == "vdu" ]:
        if fetch(i,["service-function","vdu","id"]) == id:
            return fetch(i,["service-function","vdu","epa-attributes"])

def fetch(adict,info):
    try:
        info.insert(0,adict)
        return reduce( (lambda x, y: x.get(y)) ,info)
    except:
         return None

## Function that extracts information from a reply from an agent.
def extract_info_agent_message(msg):
    info = {
        'dc-part-id' : copy.deepcopy(fetch(msg,["dc-slice-part","name"])),
        'provider' : copy.deepcopy(fetch(msg,["dc-slice-part","dc-slice-controller", "dc-slice-provider"])),
        'cost' : copy.deepcopy(fetch(msg,["dc-slice-part","cost"])),
        'pop-ip' : copy.deepcopy(fetch(msg,["dc-slice-part","dc-slice-point-of-presence", "ip"]))
    }
    return info

# Link related Code
# extract slice parts
def fetch_net_slice_parts(slice_description):
    messages = [net_populate(net_spm,slice_description) for net_spm in slice_description["slice"] \
               if "net-slice-part" in net_spm]
    return messages

# Populates the request with constraints.
def net_populate(net_spm,slice_description):
    out_message = copy.deepcopy(net_spm)
    #out_message[""]  = []
    out_message["net-slice-part"]["cost"] = {}
    out_message["net-slice-part"]["cost"]["net-model"] = copy.deepcopy(fetch(slice_description,["cost","net-model"]))
    for srv_elem in fetch(out_message,["net-slice-part","accommodates"]):
        # print(srv_elem["service-element"])
        # Replaces the Service element name with the service-link descriptions
        srv_elem["service-element"] = fetch_link_cons(srv_elem["service-element"],slice_description)
    #print(out_message)
    return out_message

# fecthes constraints from the slicedescription that concern a particular
# service element.
def fetch_link_cons(id,slice_des):
    listofservices = fetch(slice_des,["service"])
    for i in [ x for x in listofservices if  "service-link" in x] :#fetch(x,["service-function","service-element-type"]) == "vdu" ]:
        if fetch(i,["service-link","link","name"]) == id:
            return fetch(i,["service-link","link"])

# Creates Messages ONLY addressed to different providers
# UGLY.
def create_messages_to_net_agents(slice_description,dc_parts_info):
    net_slice_parts = fetch_net_slice_parts(slice_description)
    outmessages = []
    for netp in net_slice_parts:
        parts = fetch(netp,["net-slice-part","links"])
        part1 = [part["dc-part1"] for part in parts if "dc-part1" in part][0]
        part2 = [part["dc-part2"] for part in parts if "dc-part2" in part][0]
        for ans1 in dc_parts_info:
            if fetch(ans1,["dc-part-id"]) == part1:
                for ans2 in dc_parts_info:
                    if ans1["provider"] == ans2["provider"]:
                        continue
                    if fetch(ans2,["dc-part-id"]) == part2:
                        outmessages.append(add_ips(copy.deepcopy(netp),ans1,ans2,part1,part2))
    return outmessages
# Adds the ips of the providers and their names (for debugging) to
# the message addressed to agents.
def add_ips(msg,ans1,ans2,part1,part2):
    msg["net-slice-part"]["link-ends"]["link-end1-name"]=part1
    msg["net-slice-part"]["link-ends"]["link-end1-ip"]=ans1["pop-ip"]
    msg["net-slice-part"]["link-ends"]["link-end1-provider"]=ans1["provider"]
    msg["net-slice-part"]["link-ends"]["link-end2-name"]=part2
    msg["net-slice-part"]["link-ends"]["link-end2-ip"]=ans2["pop-ip"]
    msg["net-slice-part"]["link-ends"]["link-end2-provider"]=ans2["provider"]
    return msg
