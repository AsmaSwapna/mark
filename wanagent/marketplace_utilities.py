# Some common to  all utilities.
import json

def loadFile(filename):
    with open(filename, 'r') as f:
        datastore = json.load(f)
    return datastore

def load_rabbit_config():
    return loadFile('./rabbitmq_config.json')


## For testing
if __name__ == "__main__":
    d = load_rabbit_config()
    print(json.dumps(d))
