#!/bin/bash
FONT=12
xterm -fa 'Monospace' -fs $FONT -T "Europe WAN" +ls -xrm 'XTerm*selectToClipboard: true' -hold -e python3 -B netagent.py "./netProv1.json" "wan" &
xterm -fa 'Monospace' -fs $FONT -T "America WAN" +ls -xrm 'XTerm*selectToClipboard: true' -hold -e python3 -B netagent.py "./netProv2.json" "wan" &
xterm -fa 'Monospace' -fs $FONT -T "Europe WAN2" +ls -xrm 'XTerm*selectToClipboard: true' -hold -e python3 -B netagent.py "./netProv3.json" "wan"
