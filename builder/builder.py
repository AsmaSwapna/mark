# Partial Implementation of the Slice Builder
# Developed under the Necos Project (http://www.h2020-necos.eu/)
# Authors: Ilias Sakellariou, Sarantis Kalafatidis, Polyxronis Valsamas, Lefteris Mamatas
# University of Macedonia, Thessaloniki Greece.

import os
import json
import uuid
import copy
import pika
import sys
from functools import reduce
from marketplace_utilities import *
from slice_selection import *
import time

##################################################
# signal section
import signal
def terminateAgent(f1,f2):
    sys.exit(1)

signal.signal(signal.SIGHUP, terminateAgent)
signal.signal(signal.SIGTERM, terminateAgent)
signal.signal(signal.SIGINT, terminateAgent)
###################################################


RMQ_Conf = load_rabbit_config()
hostIP=RMQ_Conf['rabbitmq_ip'] #'127.0.0.1'
creds = pika.PlainCredentials(RMQ_Conf['rabbitmq_user'],RMQ_Conf['rabbitmq_pass'])
#hostIP='172.16.5.160'
#creds = pika.PlainCredentials('agent','agent')

# Don't like global vars by will do for now
request_slice_graph = {}
start_time = 0

def main():
    #os.system("yml2json builder_to_broker_complete.yaml --output builder_to_broker_complete.json")
    #jsonfile = loadFile('builder_to_broker_complete.json')
    jsonfile = loadFile(sys.argv[1])

    global request_slice_graph
    global start_time
    request_slice_graph = extract_slice_graph(jsonfile)
    #prepare for sending
    message=json.dumps(jsonfile)
    result = channel.queue_declare('',exclusive=True)
    callback_queue = result.method.queue
    sendToBroker(message,callback_queue)
    start_time = time.monotonic()
    start_consuming(callback_queue)
    print('end')


#-----Request to agents------
def sendToBroker(message,callback_queue):
    #corr_id = str(uuid.uuid4())
    channel.basic_publish(
        exchange='',
        routing_key= "rpc_queue",
        properties=pika.BasicProperties(
            reply_to=callback_queue,
            #correlation_id="3",
        ),
        body=message)

def start_consuming(callback_queue):
    #-- Basic consume (waiting for responce)
    channel.basic_consume(queue=callback_queue,
                on_message_callback=on_response,
                auto_ack=True)
    channel.start_consuming()

#-----What to do when i recieve a message from agents
def on_response(ch, method, props, body):
    print('Wall Time Elapsed,', time.monotonic() - start_time,',', end = '')
    response = body.decode()
    ch.stop_consuming()
    process_response(response)


# Function that processes the reply from the broker.
def process_response(response):
    # List of json obects each corresponding to a slice part alternative
    message_list = [json.loads(p) for p in eval(response)]
    #print_replies(message_list)
    slice_selection(message_list,request_slice_graph)





if __name__ == "__main__":
    #---------------- Connection ----------------------

    #----connecting with RabbitMQ server----
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=hostIP, credentials = creds))
    channel = connection.channel()
    main()
    connection.close()
