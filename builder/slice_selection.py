# Code to extract the best slices
# Developed under the Necos Project (http://www.h2020-necos.eu/)
# Authors: Ilias Sakellariou, Sarantis Kalafatidis, Polyxronis Valsamas, Lefteris Mamatas
# University of Macedonia, Thessaloniki Greece.

import json
import copy
from functools import reduce
from marketplace_utilities import *

# Limit for computing Slice Parts
LIMIT = 50000
Alt_slices = 0
Alt_slices_list = []

# Main used for development
def main():
    print("Development Mode")
    request = loadFile('builder_to_broker_complete.json')
    slice_graph = extract_slice_graph(request)
    print(slice_graph)
    #prepare for sending
    #print(json.dumps(request))
    # Loads a file from the parts
    message_list = loadFile('answer.json')
    #printlist(message_list["dc-slice-parts"])
    #printlist(message_list["net-slice-parts"])
    process_replies_from_broker(message_list, slice_graph)

# Top level Slice Selection  Function
def slice_selection(broker_message_list,request_slice_graph):
    ans = statistics(broker_message_list)
    process_replies_from_broker(ans,request_slice_graph)

def process_replies_from_broker(message_list,slice_graph):
    # Annotates all links with a cost.
    annotated_links = annotated_links_with_cost(message_list)
    # Creates a list of alternative slice graphs
    try:
        g = select_parts(slice_graph,annotated_links)
        print("Number of Alternative Slices,", len(g))
        if g:
            final = min_cost_slice(g)
            print("Final Selection: ")
            print_selection_info(final)
    except LimitReached as exept:
        print(exept.args,',',str(LIMIT))
        final = min_cost_slice(Alt_slices_list)
        print("Final Selection: ")
        print_selection_info(final)



################################################################################
def print_replies(message_list):
    for part_dict in message_list:
            print(json.dumps(extract_info_message(part_dict)))


# statistics function
# counts number of messages in the repsonse received.
# and reports back interesting info from messages
def statistics(replies_list):
    answer = {}
    for what in ["dc-slice-part", "net-slice-part"]:
        parts = [extract_info_message(m) for m in replies_list if what in m]
        answer[what+"s"] = parts
        print("Rec-"+what+"s,", len(parts),',',end='', flush = True)
    return answer

## Extracting Information from Replies
def extract_info_message(msg):
    if "dc-slice-part" in msg:
        return extract_info_agent_message(msg)
    if "net-slice-part" in msg:
        return extract_info_net_message(msg)
    return "NONE"


def extract_info_agent_message(msg):
    info = {
        'dc-part-id' : copy.deepcopy(fetch(msg,["dc-slice-part","name"])),
        'provider' : copy.deepcopy(fetch(msg,["dc-slice-part","dc-slice-controller", "dc-slice-provider"])),
        'cost' : copy.deepcopy(fetch(msg,["dc-slice-part","cost"])),
        'pop-ip' : copy.deepcopy(fetch(msg,["dc-slice-part","dc-slice-point-of-presence", "ip"]))

    }
    return info

def extract_info_net_message(msg):
    info = {
      'net-part-id' : copy.deepcopy(fetch(msg,["net-slice-part","name"])),
      'provider' :  copy.deepcopy(fetch(msg,["net-slice-part","wan-slice-controller","wan-slice-provider"])),
      'cost' : copy.deepcopy(fetch(msg,["net-slice-part","cost"])),
      'link-ends' : copy.deepcopy(fetch(msg,["net-slice-part","link-ends"])),
    }
    return info

###############################################################################

# Function that annotates a link with its total overall cost, i.e. adds the cost
# of the link itself and its connected dc-slice-parts.
def annotated_links_with_cost(message_list):
    dc_parts = message_list["dc-slice-parts"]
    net_parts = message_list["net-slice-parts"]
    ans = []
    for npart in net_parts:
        res = {"net-part-id":npart["net-part-id"], "provider":npart["provider"]}
        #res = copy.deepcopy(npart)
        total_cost = npart["cost"]
        res["links"]={}
        for loc in ["1","2"]:
            name = npart["link-ends"]["link-end"+loc+"-name"]
            provider = npart["link-ends"]["link-end"+loc+"-provider"]
            ip =  npart["link-ends"]["link-end"+loc+"-ip"]
            total_cost += search_cost(dc_parts,name,provider,ip)
            res["links"].update({name:provider})
        res["total_cost"]=total_cost
        ans.append(res)
        #print(res)
    return ans


# Function that returns the cost of a slice part with the
# given name, provider, ip.
def search_cost(dc_parts_list,name,provider,ip):
    for part in dc_parts_list:
        if part["dc-part-id"] == name and \
           part["provider"] == provider and \
           part["pop-ip"] == ip:
           return part["cost"]
    print("Error Provider not Found!", "(",name, provider,")")
    return -1

###############################################################################
# Creating Alternative slices
# Search Structure representation
# SLICE GRAPHS
## Exctracts the Slice Graph
# The Slice graph is the representation of the slice, as described in
# the original request. The slice graph representation is :
# {"dc_parts":{<slice parts description>},
#   "net_parts":[<net_parts>] }
# This is an example ofn te slice Graph
# {'dc_parts':
#   {'dc-slice1': 'undef', 'dc-slice2': 'undef', 'dc-slice3': 'undef'},
#  'net_parts':
#   [{'name': 'pop-dc-slice1-to-pop-dc-slice2', 'dc-part1': 'dc-slice1', 'dc-part2': 'dc-slice2', 'provider': 'undef'},
#    {'name': 'pop-dc-slice1-to-pop-dc-slice3', 'dc-part1': 'dc-slice1', 'dc-part2': 'dc-slice3', 'provider': 'undef'}]}

def extract_slice_graph(slice_request):
    parts_list = slice_request["slices"]["sliced"]["slice"]
    dc_parts = {part["dc-slice-part"]["name"]:"undef" for part in parts_list if "dc-slice-part" in part}
    net_parts = [pop_net(part) for part in parts_list if "net-slice-part" in part]
    #services = [(part["dc-slice-part"]["name"],len(part["dc-slice-part"]["vdus"])) for part in parts_list if "dc-slice-part" in part]
    services = [len(part["dc-slice-part"]["vdus"]) for part in parts_list if "dc-slice-part" in part]
    #print("serv:",reduce(lambda x,y: x+y,services))
    print("DC Slice Parts,", len(dc_parts),',', end='')
    print("Net Slice Parts,", len(net_parts),',', end='', flush=True)
    print("Services,",reduce(lambda x,y: x+y,services),',',end='',flush=True)
    #print("Total Services", )
    slice_graph = {"dc_parts":dc_parts, "net_parts":net_parts}
    return slice_graph

### Create a concise slice graph.
def pop_net(part):
    net = {"name": part["net-slice-part"]["name"]}
    for i in (part["net-slice-part"]["links"]):
        net.update(i)
    net["provider"]="undef"
    return net

# Returns the minimum cost slice out of all the alternatives
# graphs reported by the search function.
def min_cost_slice(slice_graph_list):
    return reduce(min_cost_slice_g,slice_graph_list)

# Comparing two Slice GRAPHS (used in the reduce)
def min_cost_slice_g(sg1,sg2):
    if sg1["cost"] < sg2["cost"]:
        return sg1
    else:
        return sg2

###############################################################################
# Search Process
# selecting a Slice part
def select_parts(slice_graph,annot):
    links = slice_graph["net_parts"]
    link_names = [n["name"] for n in links]
    slice_graph["cost"] = 0
    return create_alternative_slices(link_names,slice_graph,annot)

# create_alternative_slices(links,slice_graph,annotated_links)
# Function that creates all valid slices out of the annotated links.
# The list created contains all alternatives.
# Is this dfs in python? (where is my Prolog?)
def create_alternative_slices(link_names,slice_graph,annot):
    global Alt_slices
    global Alt_slices_list
    if not link_names:
        Alt_slices = Alt_slices + 1
        if Alt_slices > LIMIT:
            raise LimitReached("reached " + str(Alt_slices))
        else:
            Alt_slices_list.append(slice_graph)
            return [slice_graph]
    name = link_names.pop(0)
    # gather alternatives
    alts = [n for n in annot if n["net-part-id"] == name]
    # DEBUG print("for ", name, "there are", len(alts))
    children = [is_allowed_net_part(netp,slice_graph) for netp in alts]
    #print("--------------", name)
    # DEBUG printlist(children)
    #print("--------------------")
    #print(children)
    ans = []
    for c in children:
        if c != False:
                p = create_alternative_slices(copy.deepcopy(link_names),c,annot)
                if p:
                    ans.extend(p)
    return ans
    #return [select_net_part(link_names,c,annot) for c in children if c != False]

# One way pattern matching for dc-slices
# Checks if the link can be added.
def is_allowed_net_part(net_part,slice_graph):
    rep = copy.deepcopy(slice_graph)
    dc_instances = rep["dc_parts"]
    #print(net_part["links"])
    #for (k,v) in net_part["links"].items():
    #    print(k," ",v)
    for (key,value) in net_part["links"].items():
        if dc_instances[key] == "undef":
            dc_instances[key] = value
        elif dc_instances[key] != value:
            # DEBUG print("FALSE: ",key,value,dc_instances[key])
            return False
    # Checks DONE
    for np in rep["net_parts"]:
        if np["name"] == net_part["net-part-id"]:
            np["provider"]=net_part["provider"]
            break
    rep["cost"] += net_part["total_cost"]
    return rep

###############################################################################


class LimitReached(Exception):
    def __init__(self, message):
        self.message = message


# Printing a list.
def printlist(list):
    for el in list:
        print(el)

# Printing nicely the Selections
def print_selection_info(sp):
     DC_providers = sp["dc_parts"].items()
     printlist(DC_providers)
     NET_Providers = [(p["name"],p["provider"]) for p in sp["net_parts"]]
     printlist(NET_Providers)
     print("cost: ", sp["cost"])





if __name__ == "__main__":
    main()
